package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
    }

    private fun init(){
        auth = Firebase.auth
        LogIn()
    }

    private fun LogIn(){
        val loginmail = shesvla.text.toString()
        val loginpassword = parolishesvla.text.toString()
        if (loginmail.isNotEmpty() && loginpassword.isNotEmpty())
            loginprogressbar.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(loginmail, loginpassword)
                .addOnCompleteListener(this) { task ->
                    loginprogressbar.visibility = View.GONE
                    if (task.isSuccessful) {
                        d("login", "signInWithEmail:success")
                        Toast.makeText(this, "Authentication is Success", Toast.LENGTH_SHORT)
                            .show()
                    }else{
                        d("LogIn", "SignInWithEmail:Failure", task.exception)
                        Toast.makeText(baseContext, "${task.exception}", Toast.LENGTH_SHORT).show()
                    }
                }
    }
}