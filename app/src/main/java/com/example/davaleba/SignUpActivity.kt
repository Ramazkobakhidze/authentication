package com.example.davaleba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
        d("method", "onCreate")
    }

    override fun onStart() {
        super.onStart()
        d("method", "onStart")
    }

    override fun onResume() {
        super.onResume()
        d("method", "onResume")
    }

    override fun onPause() {
        super.onPause()
        d("method", "onPause")
    }

    override fun onStop() {
        super.onStop()
        d("method", "onStop")
    }

    override fun onRestart() {
        super.onRestart()
        d("method", "onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        d("method", "onDestroy")
    }

    private fun init() {
        auth = Firebase.auth
        daregistrireba.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        val mail = registracamaili.text.toString()
        val password = registraciaparoli.text.toString()
        val repeatpassword = parolisganmeoreba.text.toString()

        if (mail.isNotEmpty() &&
            password.isNotEmpty() && repeatpassword.isNotEmpty()
        ) {
            if (password == repeatpassword) {
                deleteClick(true)
                auth.createUserWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(this) { task ->
                        deleteClick(false)
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("Sign Up Success", "createUserWithEmail:success")
                            val user = auth.currentUser
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Sign Up Failed", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        // ...
                    }
            }
        }
    }
    private fun deleteClick(isStarted:Boolean){
        daregistrireba.isClickable = !isStarted
        if(isStarted)
            ProgressBar.visibility = View.VISIBLE
        else
            ProgressBar.visibility = View.GONE
    }

    private fun openHomeScreen(){
        val intent = Intent(this, HomeScreenActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}